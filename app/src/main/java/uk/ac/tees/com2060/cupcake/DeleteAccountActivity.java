package uk.ac.tees.com2060.cupcake;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.EmailAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

/**
 * Delete Account Activity.
 */
public class DeleteAccountActivity extends AppCompatPreferenceActivity
{
    FirebaseUser user;
    FirebaseAuth auth;
    public EditText pass;
    private AuthCredential cred;
    public Button btnDelete;

    /**
     * Initializes activity.
     * @param savedInstanceState
     */
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        // Setting the view
        setContentView(R.layout.activity_delete_account);

        btnDelete = (Button)findViewById(R.id.btn_deleteAccount);
        pass = (EditText) findViewById(R.id.et_password);

        // Get unique identifier of current user
        user = FirebaseAuth.getInstance().getCurrentUser();

        btnDelete.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                //authenticate user
                cred = EmailAuthProvider.getCredential(user.getEmail(), pass.getText().toString());
                user.reauthenticate(cred).addOnCompleteListener(new OnCompleteListener<Void>() {
                    @Override
                    public void onComplete(@NonNull Task<Void> task)
                    {
                        // If wrong password is entered then display a message to the user.
                        // Else code to handle deleting account called
                        if(!task.isSuccessful())
                        {
                            Toast.makeText(DeleteAccountActivity.this, "Incorrect Password", Toast.LENGTH_SHORT).show();
                        }
                        else
                        {
                            user.delete().addOnCompleteListener(new OnCompleteListener<Void>()
                            {
                            @Override
                            public void onComplete(@NonNull Task<Void> task)
                            {
                                // If task is successful then account deleted in user database
                                // Else account failed to delete and a message is displayed to user
                                if (task.isSuccessful())
                                {
                                    Toast.makeText(DeleteAccountActivity.this, "Your profile is deleted:( Create a account now!", Toast.LENGTH_SHORT).show();
                                }
                                else
                                {
                                    Toast.makeText(DeleteAccountActivity.this, "Failed to delete your account!", Toast.LENGTH_SHORT).show();
                                }
                            }
                            });
                        }

                    }
                });
            }
        });
    }
}
