package uk.ac.tees.com2060.cupcake;

import java.io.Serializable;

/**
 * Model class used to represent a single job
 */
class UserJob implements Serializable
{
    /**
     * Attributes of UserJob
     */
    private String dbRef;
    private String poster;
    private String title;
    private String desc;
    private double weight;
    private JAddress pAdd;
    private JAddress dAdd;
    private String lowBid;
    private String status, acceptedBidder;

    /**
     * Empty constructor
     */
    public UserJob(){}

    /**
     * Main constructor
     * @param po
     * @param t
     * @param d
     * @param w
     * @param s
     * @param ab
     */
    public UserJob(String po,String t, String d, double w, String s, String ab)
    {
        poster = po;
        title = t;
        desc = d;
        weight = w;
        status = s;
        acceptedBidder = ab;
    }

    /**
     * Sets the dbRef
     * @param r
     */
    public void setDbRef(String r)
    {
        dbRef = r;
    }

    /**
     * Sets the pAdd
     * @param a
     */
    public void setPAdd(JAddress a)
    {
        pAdd = a;
    }

    /**
     * Sets the dAdd
     * @param a
     */
    public void setDAdd(JAddress a)
    {
        dAdd = a;
    }

    /**
     * Sets the lowBid
     * @param l
     */
    public void setLowBid(String l)
    {
        lowBid = l;
    }

    /**
     * Sets the status
     * @param s
     */
    public void setStatus(String s)
    {
        status = s;
    }

    /**
     * Sets the acceptedBidder
     * @param acceptedBid
     */
    public void setAcceptedBidder(String acceptedBid) {
        this.acceptedBidder = acceptedBid;
    }


    /**
     * Retrieves the dbRef
     * @return dbRef
     */
    public String getDbRef()
    {
        return dbRef;
    }

    /**
     * Retrieves the poster
     * @return poster
     */
    public String getPoster()
    {
        return poster;
    }

    /**
     * Retrieves the title
     * @return title
     */
    public String getTitle()
    {
        return title;
    }

    /**
     * Retrieves the description
     * @return desc
     */
    public String getDesc()
    {
        return desc;
    }

    /**
     * Retrieves the weight
     * @return weight
     */
    public double getWeight()
    {
        return weight;
    }

    /**
     * Retrieves the pickup address
     * @return pAdd
     */
    public JAddress getPAdd()
    {
        return pAdd;
    }

    /**
     * Retrieves the delivery address
     * @return dAdd
     */
    public JAddress getDAdd()
    {
        return dAdd;
    }

    /**
     * Retrieves the lowest bidder
     * @return lowBid
     */
    public String getLowBid() {
        return lowBid;
    }

    /**
     * Retrieves the status
     * @return status
     */
    public String getStatus()
    {
        return status;
    }

    /**
     * Retrieves the accepted bidder
     * @return acceptedBidder
     */
    public String getAcceptedBidder() {
        return acceptedBidder;
    }
}
