package uk.ac.tees.com2060.cupcake;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

/**
 * Created by s6100549 on 30/04/18.
 */

public class UsersRecyclerViewAdapter extends RecyclerView.Adapter<UsersRecyclerViewAdapter.ViewHolder>
{
    private List<User> mUsersList;
    private Context mContext;


    // Provide a reference to the views for each data item
    // Complex data items may need more than one view per item, and
    // you provide access to all the views for a data item in a view holder
    public class ViewHolder extends RecyclerView.ViewHolder
    {
        //this method gets the name and the first inital for the user listing
        public TextView personNameTxtV;
        public TextView personIntitalTxtV;

        public View layout;

        public ViewHolder(View v)
        {
            super(v);
            layout = v;
            personNameTxtV = (TextView) v.findViewById(R.id.text_view_username);
            personIntitalTxtV = (TextView) v.findViewById(R.id.text_view_user_alphabet);
        }
    }

    public void add(int position, User person)
    {
        mUsersList.add(position, person);
        notifyItemInserted(position);
    }

    public void remove(int position)
    {
        mUsersList.remove(position);
        notifyItemInserted(position);
    }

    public UsersRecyclerViewAdapter(List<User> myDataset, Context context)
    {
        mUsersList = myDataset;
        mContext = context;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        //create a new view
        View view  = LayoutInflater.from(parent.getContext()).inflate(R.layout.recyclerview_users, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    //replace the contents of a view (by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, final int position)
    {
        //this method uses the name and the first inital for the user listing
        User user = mUsersList.get(position);
        holder.personNameTxtV.setText((user.getName()));
        holder.personIntitalTxtV.setText(String.valueOf(user.getName().charAt(0)));

        holder.layout.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                //send this user id to chat message activity
                Intent goToUpdate = new Intent(mContext, ChatMessagesActivity.class);
                goToUpdate.putExtra("USER_ID", mUsersList.get(position).getId());
                mContext.startActivity(goToUpdate);
            }
        });

    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount()
    {
        return mUsersList.size();
    }


}
