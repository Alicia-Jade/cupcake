package uk.ac.tees.com2060.cupcake;

import android.os.Bundle;

/**
 * Frequently Asked Questions Settings Activity.
 */

public class FAQActivity extends AppCompatPreferenceActivity
{
    /**
     * Initializes activity.
     * @param savedInstanceState
     */
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        // Setting the view and links to activity_faq.xml
        setContentView(R.layout.activity_faq);
    }
}
