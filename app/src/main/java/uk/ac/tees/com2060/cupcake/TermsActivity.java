package uk.ac.tees.com2060.cupcake;

import android.os.Bundle;

/**
 * Terms And Conditions Activity.
 */

public class TermsActivity extends AppCompatPreferenceActivity
{
    /**
     * Initializes activity.
     * @param savedInstanceState
     */
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        // Setting the view and links to activity_terms.xml
        setContentView(R.layout.activity_terms);
    }
}
