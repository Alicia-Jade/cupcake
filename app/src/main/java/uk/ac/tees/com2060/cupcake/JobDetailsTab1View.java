package uk.ac.tees.com2060.cupcake;


import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import static android.content.ContentValues.TAG;

/**
 * JobDetailsTab1View Fragment
 */
public class JobDetailsTab1View extends Fragment
{
    //Variables for database references
    DatabaseReference uDatabaseReference, bDatabaseReference,jDatabaseReference;

    //Variable for Firebase access
    FirebaseAuth auth;

    //Variables for object storage
    UserJob job;
    String id, bidId;
    UserBid bid, aBid;
    User u;
    Double lowestBid;

    //Variables for layout access
    TextView completed, progress, current, poster, lowest, jobTitle, jobDesc, jobWeight, jobPNum,
            jobPStreet, jobPAdd, jobPCity, jobPCounty, jobPPostcode, jobDNum, jobDStreet, jobDAdd,
            jobDCity, jobDCounty, jobDPostcode;
    Button btnBid, btnView, btnComplete;
    EditText etBid;
    View rootView;
    ImageButton btnViewProfile;

    /**
     * Initializes Fragment
     * @param savedInstanceState
     */
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
    }

    /**
     * Creates and returns the view hierarchy associated with the fragment
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return rootView
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        rootView = inflater.inflate(R.layout.fragment_tab1_viewjob, container, false);

        //Initialize widgets for Job Details
        poster = (TextView)rootView.findViewById(R.id.tv_viewPoster);
        lowest = (TextView)rootView.findViewById(R.id.tv_viewLowest);
        jobTitle = (TextView)rootView.findViewById(R.id.tv_viewTitle);
        jobDesc = (TextView)rootView.findViewById(R.id.tv_viewDesc);
        jobWeight = (TextView)rootView.findViewById(R.id.tv_viewWeight);

        //Initialize widgets for Pickup Address
        jobPNum = (TextView)rootView.findViewById(R.id.tv_pHouseNum);
        jobPStreet = (TextView)rootView.findViewById(R.id.tv_pStreet);
        jobPAdd = (TextView)rootView.findViewById(R.id.tv_pAdd);
        jobPCity = (TextView)rootView.findViewById(R.id.tv_pCity);
        jobPCounty = (TextView)rootView.findViewById(R.id.tv_pCounty);
        jobPPostcode = (TextView)rootView.findViewById(R.id.tv_pPostcode);

        //Initialize widgets for Delivery Address
        jobDNum = (TextView)rootView.findViewById(R.id.tv_dHouseNum);
        jobDStreet = (TextView)rootView.findViewById(R.id.tv_dStreet);
        jobDAdd = (TextView)rootView.findViewById(R.id.tv_dAdd);
        jobDCity = (TextView)rootView.findViewById(R.id.tv_dCity);
        jobDCounty = (TextView)rootView.findViewById(R.id.tv_dCounty);
        jobDPostcode = (TextView)rootView.findViewById(R.id.tv_dPostcode);

        //Initialize widgets for Information Display
        btnBid = (Button)rootView.findViewById(R.id.btn_bid);
        btnView = (Button)rootView.findViewById(R.id.btn_viewBids);
        etBid = (EditText)rootView.findViewById(R.id.et_bid);
        current = (TextView)rootView.findViewById(R.id.tv_current);
        progress= (TextView)rootView.findViewById(R.id.tv_progress);
        btnComplete = (Button)rootView.findViewById(R.id.btn_complete);
        btnViewProfile = (ImageButton)rootView.findViewById(R.id.btn_viewprofile);
        completed = (TextView)rootView.findViewById(R.id.tv_complete);

        //Call getIncomingIntent()
        getIncomingIntent();

        return rootView;
    }

    /**
     * Allows for subclass instantiation
     * @param view
     * @param savedInstanceState
     */
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);

        //Gets the id for the current user
        auth = FirebaseAuth.getInstance();
        id = auth.getCurrentUser().getUid();

        Log.d(TAG, "Lowest Bidder : " + job.getLowBid());

        setDisplay();


        //Checks if additional textviews need to be hidden
        if (jobPAdd.getText().equals(""))
        {
            jobPAdd.setVisibility(view.GONE);
        }
        if (jobPCounty.getText().equals(""))
        {
            jobPCounty.setVisibility(view.GONE);
        }
        if (jobDAdd.getText().equals(""))
        {
            jobDAdd.setVisibility(view.GONE);
        }
        if (jobDCounty.getText().equals(""))
        {
            jobDCounty.setVisibility(view.GONE);
        }

        //OnClickListener's for layout buttons
        btnBid.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                //Creates a new bid, grabbing the current user id and the entered bid and pushes it up to the database
                bid = new UserBid(job.getDbRef(), id, Double.parseDouble(etBid.getText().toString()),false);
                bidId = bDatabaseReference.push().getKey();
                bDatabaseReference.child(bidId).setValue(bid);

                //Refresh displays
                setDisplay();


            }
        });

        btnView.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                //Loads the ViewBidsActivity and passes the current job
                Intent intent = new Intent(getActivity(), ViewBidsActivity.class);
                intent.putExtra("job",job);
                startActivity(intent);
            }
        });

        btnViewProfile.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                //Loads the ViewProfileActivity and passes the current user
                Intent intent = new Intent(getActivity(), ViewProfileActivity.class);
                intent.putExtra("poster",u);
                startActivity(intent);
            }
        });

        btnComplete.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                //Initializes the database reference
                jDatabaseReference = FirebaseDatabase.getInstance().getReference("job");
                jDatabaseReference.addValueEventListener(new ValueEventListener()
                {
                    @Override
                    public void onDataChange(DataSnapshot dataSnapshot)
                    {
                        //Grabs each element for the relevant database
                        for(DataSnapshot snapshot : dataSnapshot.getChildren())
                        {
                            //Checks that the current job element matches the current job
                            if(snapshot.getKey().equals(job.getDbRef()))
                            {
                                //Grabs the current job element and updates the status to Compeleted
                                //Pushes the element back up to the database
                                job = snapshot.getValue(UserJob.class);
                                job.setStatus("Completed");
                                jDatabaseReference.child(snapshot.getKey()).setValue(job);
                            }
                        }
                    }

                    @Override
                    public void onCancelled(DatabaseError databaseError)
                    {

                    }
                });
                //Shows and hides relevant widgets
                completed.setVisibility(v.VISIBLE);
                btnComplete.setVisibility(v.GONE);
            }
        });
    }

    /**
     * Retrieves data sent with the intent in the extra field
     */
    private void getIncomingIntent(){
        Log.d(TAG, "getIncomingIntent: checking for incoming intent");

        //Checks if the intent has an extra with the reference job
        if(getActivity().getIntent().hasExtra("job"))
        {
            Log.d(TAG, "getIncomingIntent: found intent extra");

            //Grabs the data in the extra
            UserJob job = (UserJob) getActivity().getIntent().getSerializableExtra("job");

            //Calls setBid()
            setJob(job);
        }
    }

    /**
     * Deals with the data sent with the intent
     * @param j
     */
    private void setJob(UserJob j)
    {
        //Stores the data sent
        job = j;

        //Initialize the database reference
        uDatabaseReference = FirebaseDatabase.getInstance().getReference("user");
        uDatabaseReference.addValueEventListener(new ValueEventListener()
        {
            @Override
            public void onDataChange(DataSnapshot snapshot)
            {
                //Grabs each element in the relevant database
                for (DataSnapshot dataSnapshot : snapshot.getChildren())
                {
                    //Checks if the user in the database matches the poster
                    if(dataSnapshot.getKey().equals(job.getPoster()))
                    {
                        //If they match grabs the User object and sets the textView to the user's name
                        u = dataSnapshot.getValue(User.class);
                        poster.setText(u.getName());
                    }

                }

            }
            @Override
            public void onCancelled(DatabaseError databaseError)
            {

            }
        });

        //Initialize the database reference
        bDatabaseReference = FirebaseDatabase.getInstance().getReference("bid");
        bDatabaseReference.addValueEventListener(new ValueEventListener()
        {
            @Override
            public void onDataChange(DataSnapshot snapshot)
            {
                //Grabs each element in the relevant database
                for (DataSnapshot dataSnapshot : snapshot.getChildren())
                {
                    Log.d(TAG, "onDataChange: Checking for bidder");

                    //Grabs the current User Bid element
                    UserBid bid = dataSnapshot.getValue(UserBid.class);

                    //Checks if the current bid element is for the current job
                    if (bid.getJobRef().equals(job.getDbRef()))
                    {
                        //Checks if the current bid is the lowest
                        if (lowestBid == null || lowestBid > bid.getBid())
                        {
                            //Sets the textview to the current lowest bid
                            lowest.setText("£" + bid.getBid());

                            //Checks if the current user is the lowest bidder
                            if(id.equals(bid.getBidder()))
                            {
                                //Checks that the current bidder is not the accepted bidder
                                if(job.getAcceptedBidder() == null || !job.getAcceptedBidder().equals(bid.getBidder()))
                                {
                                    //Show and hide relevant widgets
                                    btnBid.setVisibility(rootView.GONE);
                                    etBid.setVisibility(rootView.GONE);
                                    btnView.setVisibility(rootView.GONE);
                                    current.setVisibility(rootView.VISIBLE);

                                }
                            }
                        }

                    }
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError)
            {

            }
        });

        //Set the job details text views
        jobTitle.setText(j.getTitle());
        jobDesc.setText(j.getDesc());
        jobWeight.setText(Double.toString(j.getWeight()));

        //Set the job pickup address text views
        jobPNum.setText(Integer.toString(j.getPAdd().getNum()));
        jobPStreet.setText(j.getPAdd().getStreet());
        jobPAdd.setText(j.getPAdd().getAdditional());
        jobPCity.setText(j.getPAdd().getCity());
        jobPCounty.setText(j.getPAdd().getCounty());
        jobPPostcode.setText(j.getPAdd().getPostcode());

        //Set the job delivery address text views
        jobDNum.setText(Integer.toString(j.getDAdd().getNum()));
        jobDStreet.setText(j.getDAdd().getStreet());
        jobDAdd.setText(j.getDAdd().getAdditional());
        jobDCity.setText(j.getDAdd().getCity());
        jobDCounty.setText(j.getDAdd().getCounty());
        jobDPostcode.setText(j.getDAdd().getPostcode());

    }

    /**
     * Hides and shows relevant widgets
     */
    public void setDisplay() {
        //Checks if the current job has been accepted
        if (job.getStatus().equals("Accepted"))
        {
            //Checks if the current user is the acceptedBidder for the job
            if (job.getAcceptedBidder().equals(id))
            {
                //Shows the complete button
                progress.setVisibility(rootView.GONE);

            }
            else
            {
                //Shows the job in progress textview
                btnComplete.setVisibility(rootView.GONE);
            }

            //Hides irrelevant widgets
            btnBid.setVisibility(rootView.GONE);
            etBid.setVisibility(rootView.GONE);
            current.setVisibility(rootView.GONE);
            btnViewProfile.setVisibility(rootView.GONE);
            btnView.setVisibility(rootView.GONE);
            completed.setVisibility(rootView.GONE);
        }
        else if(job.getStatus().equals("Completed"))
        {
            //Hides irrelevant widgets
            btnBid.setVisibility(rootView.GONE);
            etBid.setVisibility(rootView.GONE);
            current.setVisibility(rootView.GONE);
            btnViewProfile.setVisibility(rootView.GONE);
            btnView.setVisibility(rootView.GONE);
            btnComplete.setVisibility(rootView.GONE);
            progress.setVisibility(rootView.GONE);
        }
        //Checks if the current user is the job poster
        else if (id.equals(job.getPoster()))
        {
            //Hides irrelevant widgets
            btnBid.setVisibility(rootView.GONE);
            etBid.setVisibility(rootView.GONE);
            current.setVisibility(rootView.GONE);
            btnViewProfile.setVisibility(rootView.GONE);
            progress.setVisibility(rootView.GONE);
            btnComplete.setVisibility(rootView.GONE);
            completed.setVisibility(rootView.GONE);
        }
        else
        {
            //Hides irrelevant widgets
            btnView.setVisibility(rootView.GONE);
            current.setVisibility(rootView.GONE);
            progress.setVisibility(rootView.GONE);
            btnComplete.setVisibility(rootView.GONE);
            completed.setVisibility(rootView.GONE);
        }
    }
}
