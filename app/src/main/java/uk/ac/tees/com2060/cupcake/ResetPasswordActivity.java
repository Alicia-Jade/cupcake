package uk.ac.tees.com2060.cupcake;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.*;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;

/**
 * ResetPasswordActivity Class
 */
public class ResetPasswordActivity extends AppCompatActivity
{
    //Variables for layout access
    EditText inputEmail;
    Button btnReset, btnBack;
    ProgressBar progressBar;

    //Variable for Firebase access
    FirebaseAuth auth;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_reset_password);

        //Initialize widgets
        inputEmail = (EditText) findViewById(R.id.email);
        btnReset = (Button) findViewById(R.id.btn_reset_password);
        btnBack = (Button) findViewById(R.id.btn_back);
        progressBar = (ProgressBar) findViewById(R.id.progressBar);

        //Get current instance
        auth = FirebaseAuth.getInstance();

        //OnClickListener's for layout buttons
        btnBack.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                //Closes current activity
                finish();
            }
        });

        btnReset.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                //Grab the input from the editText
                String email = inputEmail.getText().toString().trim();

                //Check if the the email has been entered
                if (TextUtils.isEmpty(email))
                {
                    //Outputs error message
                    Toast.makeText(getApplication(), "Enter your registered email id", Toast.LENGTH_SHORT).show();
                    return;
                }

                //Shows while sending email
                progressBar.setVisibility(View.VISIBLE);

                //Sends email
                auth.sendPasswordResetEmail(email).addOnCompleteListener(new OnCompleteListener<Void>()
                {
                    @Override
                    public void onComplete(@NonNull Task<Void> task)
                    {
                        //Check if sending was successful and outputs the relevant message
                        if (task.isSuccessful())
                        {
                            Toast.makeText(ResetPasswordActivity.this, "We have sent you instructions to reset your password!", Toast.LENGTH_SHORT).show();
                        }
                        else
                        {
                            Toast.makeText(ResetPasswordActivity.this, "Failed to send reset email!", Toast.LENGTH_SHORT).show();
                        }

                        progressBar.setVisibility(View.GONE);
                    }
                });
            }
        });
    }
}
