package uk.ac.tees.com2060.cupcake;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import java.util.List;

import static android.content.ContentValues.TAG;

/**
 * JobsRecyclerViewAdapter Class
 */
public class JobsRecyclerViewAdapter extends RecyclerView.Adapter<JobsRecyclerViewAdapter.ViewHolder>
{
    //Variable to store context class is called from
    Context context;

    //Variable to store list of UserJobs
    List<UserJob> jobList;

    /**
     * Main constructor
     * @param context
     * @param tempList
     */
    public JobsRecyclerViewAdapter(Context context, List<UserJob> tempList)
    {

        this.jobList= tempList;
        this.context = context;
    }

    /**
     * ViewHolder Class
     */
    class ViewHolder extends RecyclerView.ViewHolder
    {
        //Variables used for layout access
        public TextView jobTitle;
        public TextView jobPick;
        public TextView jobDeliv;
        RelativeLayout parentLayout;

        /**
         * Main constructor
         * @param itemView
         */
        public ViewHolder(View itemView)
        {

            super(itemView);

            //Initialize widgets
            jobTitle = (TextView) itemView.findViewById(R.id.tv_viewJobTitle);
            jobPick= (TextView) itemView.findViewById(R.id.tv_viewPickup);
            jobDeliv= (TextView) itemView.findViewById(R.id.tv_delivery);
            parentLayout = itemView.findViewById(R.id.parent_layout);
        }
    }

    /**
     * Creates a new RecyclerView.ViewHolder
     * Initializes any private fields to used by RecyclerView
     * @param parent
     * @param viewType
     * @return viewHolder
     */
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {

        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recyclerview_jobs, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    /**
     * Updates the contents of RecyclerView.ViewHolder
     * @param holder
     * @param position
     */
    @Override
    public void onBindViewHolder(ViewHolder holder, final int position)
    {
        Log.d(TAG, "onBindViewHolder: called.");

        //Grabs the job at the current position
        UserJob job = jobList.get(position);

        //Sets the textviews
        holder.jobTitle.setText(job.getTitle());
        holder.jobPick.setText(job.getPAdd().getCity());
        holder.jobDeliv.setText(job.getDAdd().getCity());

        //OnClickListener for RecyclerView
        holder.parentLayout.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                //When clicked starts the JobDetailsActivity class and passes the details of the
                //clicked job to it
                Intent i = new Intent(context, JobDetailsActivity.class);
                i.putExtra("job", jobList.get(position));
                context.startActivity(i);
            }
        });


    }

    /**
     * Returns the amount of jobs
     * @return jobList.size()
     */
    @Override
    public int getItemCount()
    {

        return jobList.size();
    }

    /**
     * Interface for ClickListener
     */
    public interface ClickListener
    {
        public void itemClicked(View view ,int position);
    }
}