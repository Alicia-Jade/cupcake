package uk.ac.tees.com2060.cupcake;

import java.io.Serializable;

/**
 * Model class used to represent a LatLng location object
 */

public class LatLng implements Serializable
{
    /**
     * Attributes for LatLng
     */
    private Double latitude;
    private Double longitude;

    /**
     * Empty constructor
     */
    public LatLng() {}

    /**
     * Main constructor
     * @param lat
     * @param lon
     */
    public LatLng(Double lat, Double lon)
    {
        latitude = lat;
        longitude = lon;
    }

    /**
     * Retrieves the latitude
     * @return latitude
     */
    public Double getLatitude() {
        return latitude;
    }

    /**
     * Retrieves the longitude
     * @return longitude
     */
    public Double getLongitude() {
        return longitude;
    }

    /**
     * Sets the latitude value
     * @param latitude
     */
    public void setLatitude(Double latitude) {
        this.latitude = latitude;
    }

    /**
     * Sets the longitude value
     * @param longitude
     */
    public void setLongitude(Double longitude) {
        this.longitude = longitude;
    }
}
