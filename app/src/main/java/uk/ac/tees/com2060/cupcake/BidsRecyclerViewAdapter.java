package uk.ac.tees.com2060.cupcake;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.List;

import static android.content.ContentValues.TAG;

/**
 * BidsRecyclerViewAdapter Class
 */
public class BidsRecyclerViewAdapter extends RecyclerView.Adapter<BidsRecyclerViewAdapter.ViewHolder>
{
    //Variable for database reference
    DatabaseReference uDatabaseReference;

    //Variable to store context class is called from
    Context context;

    //Variable to store list of UserBids
    List<UserBid> bids;

    /**
     * Main constructor
     * @param context
     * @param tempList
     */
    public BidsRecyclerViewAdapter(Context context, List<UserBid> tempList)
    {
        this.bids= tempList;
        this.context = context;
    }

    /**
     * ViewHolder Class
     */
    class ViewHolder extends RecyclerView.ViewHolder
    {

        //Variable used for layout access
        public TextView bidder;
        public TextView bid;
        RelativeLayout parentLayout;

        /**
         * Main constructor
         * @param itemView
         */
        public ViewHolder(View itemView)
        {
            super(itemView);

            //Initialize widgets
            bidder = (TextView) itemView.findViewById(R.id.tv_viewBidder);
            bid= (TextView) itemView.findViewById(R.id.tv_viewBid);
            parentLayout = itemView.findViewById(R.id.parent_layout);
        }
    }

    /**
     * Creates a new RecyclerView.ViewHolder
     * Initializes any private fields to be used by RecyclerView
     * @param parent
     * @param viewType
     * @return viewHolder
     */
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recyclerview_bids, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    /**
     * Updates the contents of the RecyclerView.ViewHolder
     * @param holder
     * @param position
     */
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position)
    {
        Log.d(TAG, "onBindViewHolder: called.");

        //Grabs the bid at the current position
        final UserBid currentBid = bids.get(position);

        //Initializes the database reference
        uDatabaseReference = FirebaseDatabase.getInstance().getReference("user");
        uDatabaseReference.addValueEventListener(new ValueEventListener()
        {
            @Override
            public void onDataChange(DataSnapshot snapshot)
            {
                //Grabs each element in the relevant database
                for (DataSnapshot dataSnapshot : snapshot.getChildren())
                {
                    //Checks if the user in the database matches the bidder of the current bid
                    if(dataSnapshot.getKey().equals(currentBid.getBidder()))
                    {
                        //If they match grabs the User object and sets the textView to the user's name
                        User u = dataSnapshot.getValue(User.class);
                        holder.bidder.setText(u.getName());
                    }

                }

            }
            @Override
            public void onCancelled(DatabaseError databaseError)
            {

            }
        });

        //Sets the textView the amount of the current bid
        holder.bid.setText("£" + currentBid.getBid() );

        //OnClickListener for RecyclerView
        holder.parentLayout.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                //When clicked starts the BidDetailsActivity class and passes the details of the
                //clicked bid to it
                Intent i = new Intent(context, BidDetailsActivity.class);
                i.putExtra("bid",bids.get(position));
                context.startActivity(i);
            }
        });

    }

    /**
     * Returns the amount of bids
     * @return bids.size()
     */
    @Override
    public int getItemCount()
    {

        return bids.size();
    }

    /**
     * Interface for ClickListener
     */
    public interface ClickListener
    {
        public void itemClicked(View view, int position);
    }
}
