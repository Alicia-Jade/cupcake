package uk.ac.tees.com2060.cupcake;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.firebase.auth.FirebaseAuth;

import java.io.IOException;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;


/**
 * AddJobTab2Pickup Fragment
 */
public class AddJobTab2Pickup extends Fragment
{

    //Variables for object storage
    User user;
    FirebaseAuth auth;
    private ViewPager viewPager;
    public SendMessage SM;
    private UserJob uj;

    //Variables for layout access
    private EditText etPNumber;
    private EditText etPStreet;
    private EditText etPAdd;
    private EditText etPCity;
    private EditText etPCounty;
    private EditText etPPostcode;
    private Button btnNext,btnPrev,btnCancel;

    //Variables for location services
    private static final String TAG = "AddJobTab2Pickup";
    private static final String FINE_LOCATION = android.Manifest.permission.ACCESS_FINE_LOCATION;
    private static final String COARSE_LOCATION = android.Manifest.permission.ACCESS_COARSE_LOCATION;
    private static final int LOCATION_PERMISSION_REQUEST_CODE = 1234;
    private static final int ERROR_DIALOG_REQUEST = 9001;
    private Boolean mLocationPermissionGranted = false;
    String locationPostalCode;
    List<Address> location = null;


    /**
     * Initializes Fragment
     * @param savedInstanceState
     */
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        if(isServicesOK())
        {
            getLocationPermission();
        }
    }

    /**
     * Creates and returns the view hierarchy associated with the fragment
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return rootview
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState)
    {
        View rootView = inflater.inflate(R.layout.fragment_tab2_pickup, container, false);
        return rootView;
    }

    /**
     * Allows for subclass instantiation
     * Retrieves any required widgets
     * @param view
     * @param savedInstanceState
     */
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState)
    {   super.onViewCreated(view, savedInstanceState);

        //Initialize widgets
        btnNext = (Button)view.findViewById(R.id.btn_next2);
        btnPrev = (Button)view.findViewById(R.id.btn_prev2);
        btnCancel = (Button)view.findViewById(R.id.btn_cancel2);
        viewPager = (ViewPager)getActivity().findViewById(R.id.viewPager);
        etPNumber = (EditText)view.findViewById(R.id.et_pNumber);
        etPStreet = (EditText)view.findViewById(R.id.et_pStreet);
        etPAdd = (EditText)view.findViewById(R.id.et_pAdditional);
        etPCity = (EditText)view.findViewById(R.id.et_pCity);
        etPCounty = (EditText)view.findViewById(R.id.et_pCounty);
        etPPostcode = (EditText)view.findViewById(R.id.et_pPostcode);


        //OnClickListener's for layout buttons
        btnNext.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                //Creates a new Geocoder and LatLng object
                final Geocoder gcd = new Geocoder(getContext());
                LatLng loc = null;

                //Checks that the entered postcode is in a valid format
                String regex = "^[A-Z]{1,2}[0-9R][0-9A-Z]? [0-9][ABD-HJLNP-UW-Z]{2}$";
                Pattern pattern = Pattern.compile(regex);
                locationPostalCode = etPPostcode.getText().toString().toUpperCase();
                Matcher matcher = pattern.matcher(locationPostalCode);

                if(matcher.matches())
                {
                    //If the postcode format is valid, then gets a geolocation from the entered postcode
                    try
                    {
                        location = gcd.getFromLocationName(locationPostalCode,1);
                    }
                    catch (IOException e) {

                        e.printStackTrace();
                    }

                    //Checks if a geolocation could be found
                    if(!location.isEmpty())
                    {
                        //If a location could be found, then it grabs the latitude and longitude
                        //of the location and saves it to the LatLng object
                        for(Address address : location)
                        {
                            if(address.getLocality() != null && address.getPostalCode() != null)
                            {
                                loc = new LatLng(location.get(0).getLatitude(),location.get(0).getLongitude());
                                Log.d(TAG, "Latitude : " + loc.getLatitude());
                                Log.d(TAG, "Longitude : " + loc.getLongitude());
                            }
                        }

                    }

                    //Sets the pickup address for the UserJob object
                    uj.setPAdd(new JAddress(Integer.parseInt(etPNumber.getText().toString()), etPStreet.getText().toString(), etPAdd.getText().toString(),
                            etPCity.getText().toString(), etPCounty.getText().toString(), etPPostcode.getText().toString(), loc));

                    //Passes the updated UserJob object to the next tab and displays the next tab
                    SM.sendData2(uj);
                    viewPager.setCurrentItem(2);


                }
                else
                {
                    //Informs the user if the postcode is not in a valid format
                    Toast.makeText(getContext(), "Invalid Postcode entered", Toast.LENGTH_SHORT).show();
                }

            }
        });

        btnPrev.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                //Returns to the previous tab
                viewPager.setCurrentItem(0);
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                //Goes back to the Explore Activity
                startActivity(new Intent(getActivity(), ExploreActivity.class));
            }
        });

    }

    /**
     * Interface for SendMessage
     */
    interface SendMessage
    {
        void sendData2(UserJob userj);
    }

    /**
     * Called when fragment is first attached to its context
     * @param context
     */
    @Override
    public void onAttach(Context context)
    {
        super.onAttach(context);

        try
        {
            SM = (AddJobTab2Pickup.SendMessage)getActivity();
        }
        catch(ClassCastException e)
        {
            throw new ClassCastException("Error in retrieving data. Please try again.");
        }
    }

    /**
     * Receives data from previous tab and saves it
     * @param userJ
     */
    protected void dataReceived(UserJob userJ)
    {
        uj = userJ;
    }

    /**
     * Checks that Google Location Services are working
     * @return if service is ok
     */
    public boolean isServicesOK()
    {
        Log.d(TAG, "isServicesOK: checking google services version");
        int available = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(getActivity());
        if(available == ConnectionResult.SUCCESS)
        {
            // everything is fine and user can make map requests
            Log.d(TAG, "isServicesOK: Google Play Services is working");
            return true;
        }
        else if(GoogleApiAvailability.getInstance().isUserResolvableError(available))
        {
            // an error occurred but we can resolve it
            Log.d(TAG, "isServicesOK: an error occured but we can fix it");
            Dialog dialog = GoogleApiAvailability.getInstance().getErrorDialog(getActivity(),available,ERROR_DIALOG_REQUEST);
            dialog.show();
        }
        else
        {
            Toast.makeText(getContext(), "You can't make map requests", Toast.LENGTH_SHORT).show();
        }
        return false;
    }

    /**
     * Checks that the app has permission to access the user's location
     */
    private void getLocationPermission()
    {
        Log.d(TAG, "getLocationPermission: getting location permissions");
        String [] permissions = {android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION};

        if(ContextCompat.checkSelfPermission(getContext(), FINE_LOCATION) == PackageManager.PERMISSION_GRANTED)
        {
            if(ContextCompat.checkSelfPermission(getContext(), COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED)
            {
                mLocationPermissionGranted = true;
            }
            else
            {
                ActivityCompat.requestPermissions(getActivity(), permissions, LOCATION_PERMISSION_REQUEST_CODE);
            }
        }

    }

    /**
     * Checks the result of the app permissions
     * @param requestCode
     * @param permissions
     * @param grantResults
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        Log.d(TAG, "onRequestPermissionsResult: called.");
        mLocationPermissionGranted = false;
        switch(requestCode)
        {
            case LOCATION_PERMISSION_REQUEST_CODE:
            {
                if(grantResults.length > 0)
                {

                    for(int i = 0; i <grantResults.length; i++)
                    {
                        if (grantResults[i] != PackageManager.PERMISSION_GRANTED)
                        {
                            mLocationPermissionGranted = false;
                            Log.d(TAG, "onRequestPermissionsResult: permission failed");
                            return;
                        }
                    }

                    Log.d(TAG, "onRequestPermissionsResult: permission granted");
                    mLocationPermissionGranted = true;
                }
            }
        }
    }

}

