package uk.ac.tees.com2060.cupcake;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import java.util.ArrayList;
import java.util.List;

/**
 * ViewMyJobsActivity Class
 */
public class ViewMyJobsActivity extends AppCompatActivity
{
    //Variable for database reference
    DatabaseReference databaseReference;

    //Variables for layout access
    ProgressDialog progressDialog;
    RecyclerView recyclerViewP, recyclerViewL, recyclerViewC;
    JobsRecyclerViewAdapter adapterP, adapterL, adapterC;

    //Variable for Firebase access
    FirebaseAuth auth;

    //Variables for object storage
    String id;
    List<UserJob> pList = new ArrayList<>();
    List<UserJob> lList = new ArrayList<>();
    List<UserJob> cList = new ArrayList<>();

    /**
     * Initializes Activity
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        //Sets up the required xml layout file
        setContentView(R.layout.activity_view_my_jobs);

        //Initializes the toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //Initializes the RecyclerView for the jobs in progress
        recyclerViewP = (RecyclerView)findViewById(R.id.recyclerViewProgress);
        recyclerViewP.setHasFixedSize(true);
        recyclerViewP.setLayoutManager(new LinearLayoutManager(this));

        //Initializes the RecyclerView for the live jobs
        recyclerViewL = (RecyclerView)findViewById(R.id.recyclerViewLive);
        recyclerViewL.setHasFixedSize(true);
        recyclerViewL.setLayoutManager(new LinearLayoutManager(this));

        //Initializes the RecyclerView for the completed jobs
        recyclerViewC = (RecyclerView)findViewById(R.id.recyclerViewCompleted);
        recyclerViewC.setHasFixedSize(true);
        recyclerViewC.setLayoutManager(new LinearLayoutManager(this));

        //Initializes the process dialog
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading Data");
        progressDialog.show();

        //Gets the id for the current user
        auth = FirebaseAuth.getInstance();
        id = auth.getCurrentUser().getUid();;

        //Initializes the database reference
        databaseReference = FirebaseDatabase.getInstance().getReference("job");
        databaseReference.addValueEventListener(new ValueEventListener()
        {
            @Override
            public void onDataChange(DataSnapshot snapshot)
            {

                for (DataSnapshot dataSnapshot : snapshot.getChildren())
                {
                    //Grabs the current UserJob element and sets the database ref
                    UserJob uj = dataSnapshot.getValue(UserJob.class);
                    uj.setDbRef(dataSnapshot.getKey());

                    //Checks that the current user is the job poster
                    if(id.equals(uj.getPoster()))
                    {
                        //Checks the status of the job and adds it to the relevant list
                        if(uj.getStatus().equals("Accepted"))
                        {
                            pList.add(uj);
                        }
                        else if(uj.getStatus().equals("Completed"))
                        {
                            cList.add(uj);
                        }
                        else
                        {
                            lList.add(uj);
                        }
                    }
                }

                //Sets the adapter for the jobs in progress and passes the jobs to the adapter
                adapterP = new JobsRecyclerViewAdapter(getApplicationContext(), pList);
                recyclerViewP.setAdapter(adapterP);

                //Sets the adapter for the live jobs and passes the jobs to the adapter
                adapterL = new JobsRecyclerViewAdapter(getApplicationContext(), lList);
                recyclerViewL.setAdapter(adapterL);

                //Sets the adapter for the completed jobs and passes the jobs to the adapter
                adapterC = new JobsRecyclerViewAdapter(getApplicationContext(), cList);
                recyclerViewC.setAdapter(adapterC);

                //Closes the dialog
                progressDialog.dismiss();
            }

            @Override
            public void onCancelled(DatabaseError databaseError)
            {
                //Closes the dialog
                progressDialog.dismiss();

            }
        });
    }
}
