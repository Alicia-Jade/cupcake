package uk.ac.tees.com2060.cupcake;

import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Address;
import android.location.Geocoder;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;

import java.io.IOException;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * AddJobTab3DeliveryFragment
 */
public class AddJobTab3Delivery extends Fragment
{
    //Variables for object storage
    public String jobId;
    public UserJob job;

    //Variables for layout access
    private EditText etDNumber;
    private EditText etDStreet;
    private EditText etDAdd;
    private EditText etDCity;
    private EditText etDCounty;
    private EditText etDPostcode;
    private ViewPager viewPager;
    public Button btnPost, btnPrev, btnCancel;

    //Variables for location services
    private static final String TAG = "AddJobTab2Pickup";
    private static final String FINE_LOCATION = android.Manifest.permission.ACCESS_FINE_LOCATION;
    private static final String COARSE_LOCATION = android.Manifest.permission.ACCESS_COARSE_LOCATION;
    private static final int LOCATION_PERMISSION_REQUEST_CODE = 1234;
    private static final int ERROR_DIALOG_REQUEST = 9001;
    private Boolean mLocationPermissionGranted = false;
    String locationPostalCode;
    List<Address> location;

    /**
     * Initializes Fragment
     * @param savedInstanceState
     */
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        if(isServicesOK())
        {
            getLocationPermission();
        }
    }

    /**
     * Creates and returns the view hierarchy associated with the fragment
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return rootView
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,Bundle savedInstanceState)
    {
        View rootView = inflater.inflate(R.layout.fragment_tab3_delivery, container, false);
        return rootView;
    }

    /**
     * Allows for subclass instantiation
     * Retrieves any required widgets
     * @param view
     * @param savedInstanceState
     */
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);

        //Initialize widgets
        viewPager = (ViewPager)getActivity().findViewById(R.id.viewPager);
        btnPost = (Button) view.findViewById(R.id.btn_post);
        btnPrev = (Button)view.findViewById(R.id.btn_prev3);
        btnCancel = (Button)view.findViewById(R.id.btn_cancel3);
        etDNumber = (EditText)view.findViewById(R.id.et_dNumber);
        etDStreet = (EditText)view.findViewById(R.id.et_dStreet);
        etDAdd = (EditText)view.findViewById(R.id.et_dAdditional);
        etDCity = (EditText)view.findViewById(R.id.et_dCity);
        etDCounty = (EditText)view.findViewById(R.id.et_dCounty);
        etDPostcode = (EditText)view.findViewById(R.id.et_dPostcode);

        //Initialize the job database
        final DatabaseReference jDatabase = FirebaseDatabase.getInstance().getReference("job");

        //OnClickListener's for layout buttons
        btnPost.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                //Creates a new Geocoder and LatLng object
                final Geocoder gcd = new Geocoder(getContext());
                LatLng loc = null;

                //Checks that the entered postcode is in a valid format
                String regex = "^[A-Z]{1,2}[0-9R][0-9A-Z]? [0-9][ABD-HJLNP-UW-Z]{2}$";
                Pattern pattern = Pattern.compile(regex);
                locationPostalCode = etDPostcode.getText().toString().toUpperCase();
                Matcher matcher = pattern.matcher(locationPostalCode);

                if(matcher.matches())
                {
                    try
                    {   //If the postcode format is valid, then gets a geolocation from the entered postcode
                        location = gcd.getFromLocationName(locationPostalCode,1);
                    }
                    catch (IOException e)
                    {

                        e.printStackTrace();
                    }

                    //Checks if a geolocation could be found
                    if(!location.isEmpty())
                    {
                        //If a location could be found, then it grabs the latitude and longitude
                        //of the location and saves it to the LatLng object
                        for(Address address : location)
                        {
                            if(address.getLocality() != null && address.getPostalCode() != null)
                            {
                                loc = new LatLng(location.get(0).getLatitude(),location.get(0).getLongitude());
                                Log.d(TAG, "Latitude : " + loc.getLatitude());
                                Log.d(TAG, "Longitude : " + loc.getLongitude());

                            }
                        }
                    }

                    //Generates a database key for the new job
                    jobId = jDatabase.push().getKey();

                    //Sets the delivery address for the UserJob object
                    job.setDAdd(new JAddress(Integer.parseInt(etDNumber.getText().toString()), etDStreet.getText().toString(),etDAdd.getText().toString(),
                            etDCity.getText().toString(), etDCounty.getText().toString(), etDPostcode.getText().toString(), loc));

                    //Adds the UserJob object to the database
                    jDatabase.child(jobId).setValue(job);

                    //Returns to the Explore Activity
                    startActivity(new Intent(getActivity(), ExploreActivity.class));
                }
                else
                {
                    //Informs the user if the postcode is not in a valid format
                    Toast.makeText(getContext(), "Invalid Postcode entered", Toast.LENGTH_SHORT).show();
                }
            }
        });

        btnPrev.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                //Returns to the previous tab
                viewPager.setCurrentItem(1);
            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                //Ges back to the Explore Activity
                startActivity(new Intent(getActivity(), ExploreActivity.class));
            }
        });
    }

    /**
     * Receives data from the previous tab and saves it
     * @param userJ
     */
    protected void dataReceived(UserJob userJ)
    {
        job = userJ;
    }

    /**
     * Checks that Google Location Services are working
     * @retur if service is ok
     */
    public boolean isServicesOK()
    {
        Log.d(TAG, "isServicesOK: checking google services version");
        int available = GoogleApiAvailability.getInstance().isGooglePlayServicesAvailable(getActivity());
        if(available == ConnectionResult.SUCCESS)
        {
            // everything is fine and user can make map requests
            Log.d(TAG, "isServicesOK: Google Play Services is working");
            return true;
        }
        else if(GoogleApiAvailability.getInstance().isUserResolvableError(available))
        {
            // an error occurred but we can resolve it
            Log.d(TAG, "isServicesOK: an error occured but we can fix it");
            Dialog dialog = GoogleApiAvailability.getInstance().getErrorDialog(getActivity(),available,ERROR_DIALOG_REQUEST);
            dialog.show();
        }
        else
        {
            Toast.makeText(getContext(), "You can't make map requests", Toast.LENGTH_SHORT).show();
        }
        return false;
    }

    /**
     * Checks that the app has permission ot access the user's location
     */
    private void getLocationPermission()
    {
        Log.d(TAG, "getLocationPermission: getting location permissions");
        String [] permissions = {android.Manifest.permission.ACCESS_FINE_LOCATION, android.Manifest.permission.ACCESS_COARSE_LOCATION};

        if(ContextCompat.checkSelfPermission(getContext(), FINE_LOCATION) == PackageManager.PERMISSION_GRANTED)
        {
            if(ContextCompat.checkSelfPermission(getContext(), COARSE_LOCATION) == PackageManager.PERMISSION_GRANTED)
            {
                mLocationPermissionGranted = true;
            }
            else
            {
                ActivityCompat.requestPermissions(getActivity(), permissions, LOCATION_PERMISSION_REQUEST_CODE);
            }
        }
    }

    /**
     * Checks the result of the app permissions
     * @param requestCode
     * @param permissions
     * @param grantResults
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        Log.d(TAG, "onRequestPermissionsResult: called.");
        mLocationPermissionGranted = false;
        switch(requestCode)
        {
            case LOCATION_PERMISSION_REQUEST_CODE:
            {
                if(grantResults.length > 0)
                {

                    for(int i = 0; i <grantResults.length; i++)
                    {
                        if (grantResults[i] != PackageManager.PERMISSION_GRANTED)
                        {
                            mLocationPermissionGranted = false;
                            Log.d(TAG, "onRequestPermissionsResult: permission failed");
                            return;
                        }
                    }

                    Log.d(TAG, "onRequestPermissionsResult: permission granted");
                    mLocationPermissionGranted = true;
                }
            }
        }
    }
}
