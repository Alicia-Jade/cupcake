package uk.ac.tees.com2060.cupcake;

/**
 * Model class used to represent a Review object
 */
public class Review
{
    /**
     * Attributes for Review
     */
    String review, rating, reviewer, reviewee;
    String dbRef, reviewRef;

    /**
     * Empty constructor
     */
    public Review()
    {

    }

    /**
     * Main constructor
     * @param rev
     * @param rat
     * @param revr
     * @param reve
     */
    public Review(String rev, String rat, String revr, String reve)
    {
        review = rev;
        rating = rat;
        reviewer = revr;
        reviewee = reve;
    }

    /**
     * Retrieves the review
     * @return review
     */
    public String getReview()
    {
        return review;
    }

    /**
     * Retrieves the rating
     * @return rating
     */
    public String getRating()
    {
        return rating;
    }

    /**
     * Retrieves the reviewer
     * @return reviewer
     */
    public String getReviewer()
    {
        return reviewer;
    }

    /**
     * Retrieves the reviewee
     * @return reviewee
     */
    public String getReviewee()
    {
        return reviewee;
    }

    /**
     * Retrieves the dbRef
     * @return dbRef
     */
    public String getDbRef()
    {
        return dbRef;
    }

    /**
     * Retrieves the reviewRef
     * @return reviewRef
     */
    public String getReviewRef() { return reviewRef; }
}
