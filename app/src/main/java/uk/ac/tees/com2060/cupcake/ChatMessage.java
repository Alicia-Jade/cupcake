package uk.ac.tees.com2060.cupcake;

/**
 * Created by s6100549 on 01/05/18.
 */

public class ChatMessage
{
    private String message;
    private String senderId;
    private String receiverId;

    /**
     * Model that is used for the messages
     */

    /**
     * this is an empty constructor
     */
    public ChatMessage()
    {

    }

    /**
     * these methods get and set the message, senderId and receiverId
     * these are then set in the ChatMessage() method
     * @param message
     * @param senderId
     * @param receiverId
     */
    public ChatMessage(String message, String senderId, String receiverId)
    {
        this.message = message;
        this.senderId = senderId;
        this.receiverId = receiverId;
    }

    public String getMessage()
    {
        return message;
    }

    public void setMessage(String message)
    {
        this.message = message;
    }

    public String getSenderId()
    {
        return senderId;
    }

    public void setSenderId(String senderId)
    {
        this.senderId = senderId;
    }

    public String getReceiverId()
    {
        return receiverId;
    }

    public void setReceiverId(String receiverId)
    {
        this.receiverId = receiverId;
    }
}
