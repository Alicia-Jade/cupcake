package uk.ac.tees.com2060.cupcake;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.*;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.EmailAuthProvider;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

/**
 * Change Password Activity.
 */

public class ChangePasswordActivity extends AppCompatPreferenceActivity
{
    //Variables for Firebase access
    FirebaseUser user;
    FirebaseAuth auth;
    AuthCredential cred;

    //Variables for layout access
    EditText oldpass, newpass, newpass2;
    Button btnChange;

    /**
     * Initializes activity.
     * @param savedInstanceState
     */
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        // Setting the view
        setContentView(R.layout.activity_change_password);

        btnChange = (Button)findViewById(R.id.btn_saveChanges);
        oldpass = (EditText) findViewById(R.id.et_oldPassword);
        newpass = (EditText) findViewById(R.id.et_newPassword);
        newpass2 = (EditText) findViewById(R.id.et_newPassword2);

        // Get unique identifier of current user
        user = FirebaseAuth.getInstance().getCurrentUser();

        btnChange.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                //authenticate user
                cred = EmailAuthProvider.getCredential(user.getEmail(), oldpass.getText().toString());
                user.reauthenticate(cred).addOnCompleteListener(new OnCompleteListener<Void>()
                {
                    @Override
                    public void onComplete(@NonNull Task<Void> task)
                    {
                        // If wrong password is entered then display a message to the user.
                        // Else code to handle changing the password is called
                        if(!task.isSuccessful())
                        {
                            Toast.makeText(ChangePasswordActivity.this, "Incorrect Password", Toast.LENGTH_SHORT).show();
                        }
                        else
                        {
                            if (newpass.getText().toString().equals(newpass2.getText().toString()))
                            {
                                user.updatePassword(newpass.getText().toString()).addOnCompleteListener(new OnCompleteListener<Void>()
                                {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task)
                                    {
                                        // If task is successful then password is updated in database and message displayed to user
                                        // Else password failed to update and message displayed to user
                                        if(task.isSuccessful())
                                        {
                                            Toast.makeText(ChangePasswordActivity.this, "Password updated", Toast.LENGTH_SHORT).show();
                                        }
                                        else
                                        {
                                            Toast.makeText(ChangePasswordActivity.this, "Failed to update", Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                });
                            }
                            else
                            {
                                Toast.makeText(ChangePasswordActivity.this, "Passwords doesnt match!", Toast.LENGTH_SHORT).show();
                            }

                        }

                    }
                });
            }
        });
    }
}
