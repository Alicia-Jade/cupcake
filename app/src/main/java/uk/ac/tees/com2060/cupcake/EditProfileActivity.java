package uk.ac.tees.com2060.cupcake;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.google.firebase.storage.FirebaseStorage;
import com.google.firebase.storage.StorageMetadata;
import com.google.firebase.storage.StorageReference;
import com.google.firebase.storage.UploadTask;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Edit Profile Activity.
 */
public class EditProfileActivity extends AppCompatActivity
{
    //Variable for log TAG
    private static final String TAG = "EditProfileActivity";

    //Variables for layout access
    ImageView image;
    Button edit;

    //Variable for Firebase access
    FirebaseAuth auth;

    //Variable for database reference
    DatabaseReference mUserDBRef;

    //Variable for storage reference
    StorageReference mStorageRef;

    //Variables for object storage
    byte[] byteArray = null;
    String mCurrentUserID;

    //Variables for app permissions
    static final int REQUEST_IMAGE_CAPTURE = 1;
    static final int REQUEST_IMAGE_PICK = 2;
    boolean mCameraPermissionGranted;
    boolean mGalleryPermissionGranted;
    static final String CAMERA = Manifest.permission.CAMERA;
    static final String GALLERY = Manifest.permission.WRITE_EXTERNAL_STORAGE;

    /**
     * Initializes the activity.
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        // setting the view and links to activity_update_profile.xml
        setContentView(R.layout.activity_update_profile);
        getCameraPermission();

        image = (ImageView) findViewById(R.id.userPhotoUpdate);
        edit = (Button) findViewById(R.id.updateUserProfileBtn);
        mCurrentUserID = FirebaseAuth.getInstance().getCurrentUser().getUid();

        //gets firebase storage reference
        mUserDBRef = FirebaseDatabase.getInstance().getReference("user");
        mStorageRef = FirebaseStorage.getInstance().getReference("Photos").child("Users");

        //populate views initially
        populateTheViews();

        //listen to imageview click
        image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AlertDialog.Builder builder = new AlertDialog.Builder(EditProfileActivity.this);
                builder.setTitle("Change photo");
                builder.setMessage("Choose a method to change photo");
                builder.setPositiveButton("Upload", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        pickPhotoFromGallery();
                    }
                });
                builder.setNegativeButton("Camera", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dispatchTakePictureIntent();
                    }
                });
                builder.create().show();

            }
        });

        //listen to update btn click
        edit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try
                {
                    updateUserPhoto(byteArray);
                    startActivity(new Intent(EditProfileActivity.this,ExploreActivity.class));
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }

            }
        });
    }

    /**
     * Called after onCreate.
     */
    @Override
    protected void onStart()
    {
        super.onStart();
        //populate views initially
        populateTheViews();
    }

    /**
     * Populates the image view on the activity
     */
    private void populateTheViews()
    {
        //Grabs the current user from the database
        mUserDBRef.child(FirebaseAuth.getInstance().getCurrentUser().getUid()).addValueEventListener(new ValueEventListener()
        {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot)
            {
                //Saves the current user element
                User currentuser = dataSnapshot.getValue(User.class);
                try
                {
                    //Gets the user's photo and displays it
                    String userPhoto = currentuser.getImage();
                    Picasso.with(EditProfileActivity.this).load(userPhoto).placeholder(R.mipmap.ic_launcher).into(image);
                }
                catch (Exception e)
                {
                    e.printStackTrace();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError)
            {

            }
        });
    }

    /**
     * Method to take a image using the camera.
     */
    private void dispatchTakePictureIntent()
    {
        Intent takePictureIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        if (takePictureIntent.resolveActivity(getPackageManager()) != null)
        {
            startActivityForResult(takePictureIntent, REQUEST_IMAGE_CAPTURE);
        }
    }

    /**
     * Method to pick a photo from gallery.
     */
    private void pickPhotoFromGallery()
    {
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK,MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
        if(photoPickerIntent.resolveActivity(getPackageManager())!= null)
        {
            startActivityForResult(photoPickerIntent, REQUEST_IMAGE_PICK);
        }
    }

    /**
     * Called when the activity exits, giving you the requestCode you started it with,
     * the resultCode it returned, and any additional data from it.
     * @param requestCode
     * @param resultCode
     * @param data
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK)
        {
            Bundle extras = data.getExtras();
            Bitmap imageBitmap = (Bitmap) extras.get("data");
            image.setImageBitmap(imageBitmap);

            //convert bitmap to byte array to store in firebase storage
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            imageBitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
            byteArray = stream.toByteArray();

        }
        else if(requestCode == REQUEST_IMAGE_PICK && resultCode == RESULT_OK)
        {
            if(data != null)
            {
                Bitmap imageBitmap = null;
                Uri photoUri = data.getData();
                try {
                    imageBitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(),photoUri);
                } catch (IOException e) {
                    e.printStackTrace();
                }

                Log.d(TAG, "onActivityResult: " + imageBitmap);
                image.setImageBitmap(imageBitmap);
                //convert bitmap to byte array to store in firebase storage
                ByteArrayOutputStream stream = new ByteArrayOutputStream();
                imageBitmap.compress(Bitmap.CompressFormat.JPEG, 0, stream);
                byteArray = stream.toByteArray();
            }
        }
    }

    /**
     * Method to update the users profile photo.
     * @param photoByteArray
     */
    private void updateUserPhoto(byte[] photoByteArray)
    {
        // Create file metadata with property to delete
        StorageMetadata metadata = new StorageMetadata.Builder()
                .setContentType(null)
                .setContentLanguage("en")
                .build();

        mStorageRef.child(mCurrentUserID).putBytes(photoByteArray, metadata).addOnCompleteListener(new OnCompleteListener<UploadTask.TaskSnapshot>() {
            @Override
            public void onComplete(@NonNull Task<UploadTask.TaskSnapshot> task) {
                if(!task.isSuccessful())
                {
                    //error saving photo
                }
                else
                {
                    //success saving photo
                    String userPhotoLink = task.getResult().getDownloadUrl().toString();
                    //now update the database with this user photo
                    Map<String, Object> childUpdates = new HashMap<>();
                    childUpdates.put("image", userPhotoLink);
                    mUserDBRef.child(mCurrentUserID).updateChildren(childUpdates);
                }
            }
        });
    }

    /**
     * Gets camera permission.
     */
    private void getCameraPermission()
    {
        Log.d(TAG, "getLocationPermission: getting location permissions");
        String[] permissions = {CAMERA,GALLERY};

        if(ContextCompat.checkSelfPermission(this, CAMERA) == PackageManager.PERMISSION_GRANTED)
        {
            mCameraPermissionGranted = true;
        }
        else
        {
            ActivityCompat.requestPermissions(this, permissions, REQUEST_IMAGE_CAPTURE);
        }
        if(ContextCompat.checkSelfPermission(this, GALLERY) == PackageManager.PERMISSION_GRANTED)
        {
            mGalleryPermissionGranted = true;
        }
        else
        {
            ActivityCompat.requestPermissions(this, permissions, REQUEST_IMAGE_CAPTURE);
        }

    }


    /**
     * Checks the result of the app permissions
     * @param requestCode
     * @param permissions
     * @param grantResults
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults)
    {
        Log.d(TAG, "onRequestPermissionsResult: called.");
        mCameraPermissionGranted = false;
        mGalleryPermissionGranted = false;
        switch(requestCode)
        {
            case REQUEST_IMAGE_CAPTURE:
            {
                if(grantResults.length > 0)
                {

                    for(int i = 0; i <grantResults.length; i++)
                    {
                        if (grantResults[i] != PackageManager.PERMISSION_GRANTED)
                        {
                            mCameraPermissionGranted = false;
                            Log.d(TAG, "onRequestPermissionsResult: permission failed");
                            return;
                        }
                    }
                    Log.d(TAG, "onRequestPermissionsResult: permission granted");
                    mCameraPermissionGranted = true;
                }
            }
            case REQUEST_IMAGE_PICK:
            {
                if(grantResults.length > 0)
                {
                    for(int i = 0; i <grantResults.length; i++)
                    {
                        if (grantResults[i] != PackageManager.PERMISSION_GRANTED)
                        {
                            mGalleryPermissionGranted = false;
                            Log.d(TAG, "onRequestPermissionsResult: permission failed");
                            return;
                        }
                    }

                    Log.d(TAG, "onRequestPermissionsResult: permission granted");
                    mGalleryPermissionGranted = true;
                }
            }
        }
    }
}
