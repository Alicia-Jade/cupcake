package uk.ac.tees.com2060.cupcake;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by s6100549 on 30/04/18.
 */

/**
 * this class deals with the user creating and sending a message as well as storing and pulling said messages from Firebase
 */

public class ChatMessagesActivity extends AppCompatActivity
{
    private static final String TAG = "ChatMessagesActivity";
    private RecyclerView mChatsRecyclerView;
    private LinearLayoutManager mLayoutManager;
    private EditText mMessageEditText;
    private ImageButton mSendImageButton;
    private DatabaseReference mMessagesDBRef;
    private DatabaseReference mUsersRef;
    private List<ChatMessage> mMessageList = new ArrayList<>();
    private MessagesRecyclerViewAdapter adapter = null;

    private String mReceiverId;
    private String mReceiverName;

    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_messages);

        //initialise  the views
        mChatsRecyclerView = (RecyclerView) findViewById(R.id.messagesRecyclerView);
        mMessageEditText = (EditText) findViewById(R.id.messageEditText);
        mSendImageButton = (ImageButton) findViewById(R.id.sendMessageImageButton);
        mChatsRecyclerView.setHasFixedSize(true);
        //use a linear layout manager
        mLayoutManager = new LinearLayoutManager(this);
        mLayoutManager.setStackFromEnd(true);
        mChatsRecyclerView.setLayoutManager(mLayoutManager);

        //initialise Firebase
        mMessagesDBRef = FirebaseDatabase.getInstance().getReference().child("Messages");
        mUsersRef = FirebaseDatabase.getInstance().getReference().child("user");

        //get recieverId from intent
        mReceiverId = getIntent().getStringExtra("USER_ID");
        Log.d(TAG, "mReceiverId : " + mReceiverId);

        //listen to send message imageButton click
        mSendImageButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                String message = mMessageEditText.getText().toString();
                String senderId = FirebaseAuth.getInstance().getCurrentUser().getUid();

                if (message.isEmpty())
                {
                    Toast.makeText(ChatMessagesActivity.this, "you must enter a message", Toast.LENGTH_SHORT).show();
                }
                else
                {
                    //message is entered, send
                    sendMessageToFirebase(message, senderId, mReceiverId);
                }
            }
        });
    }

    @Override
    public void onStart()
    {
        super.onStart();

        //query and populate chat messages
        queryMessages();

        //sets title bar with recipient name
        queryName(mReceiverId);
    }

    private void sendMessageToFirebase(String message, String senderId, String mReceiverId)
    {
        mMessageList.clear();

        ChatMessage newMsg = new ChatMessage(message, senderId, mReceiverId);
        mMessagesDBRef.push().setValue(newMsg).addOnCompleteListener(new OnCompleteListener<Void>()
        {
           @Override
            public void onComplete(@NonNull Task<Void> task)
           {
               if(!task.isSuccessful())
               {
                   //error
                   Toast.makeText(ChatMessagesActivity.this, "Error " + task.getException().getLocalizedMessage(), Toast.LENGTH_SHORT).show();
               }
               else
               {
                   Toast.makeText(ChatMessagesActivity.this, "Message sent successfully", Toast.LENGTH_SHORT).show();
                   mMessageEditText.setText(null);
                   hideSoftKeyboard();
               }
           }
        });
    }

    //this method hides the keyboard after the user is finished typing
    public void hideSoftKeyboard()
    {
        if(getCurrentFocus()!=null)
        {
            InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(INPUT_METHOD_SERVICE);
            inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        }
    }

    //this method pulls all the messages from Firebase
    private void queryMessages()
    {
        mMessagesDBRef.addValueEventListener(new ValueEventListener()
        {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot)
            {
                mMessageList.clear();

                for(DataSnapshot snap: dataSnapshot.getChildren())
                {
                    ChatMessage chatMessage = snap.getValue(ChatMessage.class);
                    if(chatMessage.getSenderId().equals(FirebaseAuth.getInstance().getCurrentUser()
                            .getUid()) && chatMessage.getReceiverId().equals(mReceiverId) ||
                            chatMessage.getSenderId().equals(mReceiverId) && chatMessage.getReceiverId()
                            .equals(FirebaseAuth.getInstance().getCurrentUser().getUid()))
                    {
                        mMessageList.add(chatMessage);
                    }
                }

                //populate messages
                populateMessagesRecyclerView();
            }

            @Override
            public void onCancelled(DatabaseError databaseError)
            {

            }
        });
    }

    //this  method puts the messages from firebase into the correct order
    private void populateMessagesRecyclerView()
    {
        adapter = new MessagesRecyclerViewAdapter(mMessageList, this);
        mChatsRecyclerView.setAdapter(adapter);
    }

    private void queryName(final String receiverId)
    {
        mUsersRef.child(receiverId).addValueEventListener(new ValueEventListener()
        {
            @Override
            public void onDataChange(DataSnapshot dataSnapshot)
            {
                User recipient = dataSnapshot.getValue(User.class);
                mReceiverName = recipient.getName();

                try
                {
                    getSupportActionBar().setTitle(mReceiverName);
                    getActionBar().setTitle(mReceiverName);
                }
                catch(Exception e)
                {
                    e.printStackTrace();
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError)
            {

            }
        });
    }
}
