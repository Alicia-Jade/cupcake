package uk.ac.tees.com2060.cupcake;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;
import com.google.firebase.auth.AuthCredential;
import com.google.firebase.auth.EmailAuthProvider;

/**
 * Change Email Activity.
 */
public class ChangeEmailActivity extends AppCompatPreferenceActivity
{
    //Variables for Firebase access
    FirebaseUser user;
    FirebaseAuth auth;
    AuthCredential cred;

    //Variables for layout access
    EditText confirmemail, newemail, pass;
    Button btnChange;

    /**
     * Initializes activity.
     * @param savedInstanceState
     */
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        // Setting the view and linking to activity_change_email.xml
        setContentView(R.layout.activity_change_email);

        btnChange = (Button) findViewById(R.id.btn_saveChanges);
        newemail = (EditText) findViewById(R.id.et_newEmail);
        confirmemail = (EditText) findViewById(R.id.et_confirmEmail);
        pass = (EditText) findViewById(R.id.et_pass);

        // Get unique identifier of current user
        user = FirebaseAuth.getInstance().getCurrentUser();

        btnChange.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                //authenticate user
                cred = EmailAuthProvider.getCredential(user.getEmail(), pass.getText().toString());
                user.reauthenticate(cred).addOnCompleteListener(new OnCompleteListener<Void>()
                {
                    @Override
                    public void onComplete(@NonNull Task<Void> task)
                    {
                        // If wrong password is entered then display a message to the user.
                        // Else code to handle changing email address is called
                        if (!task.isSuccessful())
                        {
                            Toast.makeText(ChangeEmailActivity.this, "Incorrect Password", Toast.LENGTH_SHORT).show();
                        }
                        else
                        {
                            // If the email addresses in both new email and confirm email match then code to change email address is called
                            // Else
                            if (newemail.getText().toString().equals(confirmemail.getText().toString()))
                            {
                                user.updateEmail(newemail.getText().toString().trim()).addOnCompleteListener(new OnCompleteListener<Void>()
                                {
                                    @Override
                                    public void onComplete(@NonNull Task<Void> task)
                                    {
                                        // If task is successful then email address is changed and message displayed to user
                                        // Else email address failed to update and message displayed to user
                                        if (task.isSuccessful())
                                        {
                                            Toast.makeText(ChangeEmailActivity.this, "Email address is updated.", Toast.LENGTH_LONG).show();
                                        }
                                        else
                                        {
                                            Toast.makeText(ChangeEmailActivity.this, "Failed to update email!", Toast.LENGTH_LONG).show();
                                        }
                                    }
                                });
                            }
                            else
                            {
                                Toast.makeText(ChangeEmailActivity.this, "Email address doesnt match!", Toast.LENGTH_SHORT).show();
                            }
                        }

                    }

                });
            }
        });
    }
}
