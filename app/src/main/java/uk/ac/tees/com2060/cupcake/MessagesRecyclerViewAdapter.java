package uk.ac.tees.com2060.cupcake;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.firebase.auth.FirebaseAuth;

import java.util.List;

/**
 * Created by s6100549 on 01/05/18.
 */

public class MessagesRecyclerViewAdapter extends RecyclerView.Adapter<MessagesRecyclerViewAdapter.ViewHolder>
{
    public static final int ITEM_TYPE_SENT = 0;
    public static final int ITEM_TYPE_RECEIVED = 1;

    private List<ChatMessage> mMessagesList;
    private Context mContext;

    // provide a reference to the views for each data item
    // complex data items may need more than one view per item
    // provide access to all the views for a data item in a view holder

    //this is the viewholder for the user messages
    public class ViewHolder extends RecyclerView.ViewHolder
    {
        public TextView messageTextView;

        public View layout;

        public ViewHolder(View v)
        {
            super(v);
            layout = v;
            messageTextView = (TextView) v.findViewById(R.id.chatMsgTextView);
        }
    }

    //this is for the other users messages
    public class ViewHolder2 extends RecyclerView.ViewHolder
    {
        public TextView messageTextView;

        public View layout;

        public ViewHolder2(View v)
        {
            super(v);
            layout = v;
            messageTextView = (TextView) v.findViewById(R.id.chatMsgTextView);

        }
    }

    public void add(int position, ChatMessage message)
    {
        mMessagesList.add(position, message);
        notifyItemInserted(position);
    }

    public void remove(int position)
    {
        mMessagesList.remove(position);
        notifyItemRemoved(position);
    }

    public MessagesRecyclerViewAdapter(List<ChatMessage> myDataset, Context context)
    {
        mMessagesList = myDataset;
        mContext = context;
    }

    @Override
    public int getItemViewType(int position)
    {
        if(mMessagesList.get(position).getSenderId().equals(FirebaseAuth.getInstance().getCurrentUser().getUid()))
        {
            return ITEM_TYPE_SENT;
        }
        else
        {
            return ITEM_TYPE_RECEIVED;
        }
    }

    // Create new views (invoked by the layout manager)
    @Override
    public MessagesRecyclerViewAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View v = null;
        if (viewType == ITEM_TYPE_SENT)
        {
            v = LayoutInflater.from(mContext).inflate(R.layout.sent_msg_row, null);
        }
        else if (viewType == ITEM_TYPE_RECEIVED)
        {
            v = LayoutInflater.from(mContext).inflate(R.layout.received_msg_row, null);
        }
        return new ViewHolder(v); // view holder for header items
    }

    // Replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(ViewHolder holder, int position)
    {
        // - get element from your dataset at this position
        // - replace the contents of the view with that element
        ChatMessage msg = mMessagesList.get(position);
        holder.messageTextView.setText(msg.getMessage());


    }

    // Return the size of your dataset (invoked by the layout manager)
    @Override
    public int getItemCount()
    {
        return mMessagesList.size();
    }
}
