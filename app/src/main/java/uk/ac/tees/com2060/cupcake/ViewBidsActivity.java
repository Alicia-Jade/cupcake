package uk.ac.tees.com2060.cupcake;

import android.app.ProgressDialog;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import java.util.ArrayList;
import java.util.List;
import static android.content.ContentValues.TAG;

/**
 * ViewBidsActivity Class
 */
public class ViewBidsActivity extends AppCompatActivity
{
    //Variable for database reference
    DatabaseReference bDatabaseReference;

    //Variables for layout access
    ProgressDialog progressDialog;
    BidsRecyclerViewAdapter bAdapter;
    RecyclerView recyclerView;

    //Variables for object storage
    UserJob job;
    List<UserBid> bids = new ArrayList<>();
    List<Review> reviews = new ArrayList<>();

    /**
     * Initializes Activity
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        //Sets up the required xml layout file
        setContentView(R.layout.activity_view_bids);

        //Initializes the toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        //Calls getIncomingIntent()
        getIncomingIntent();

        //Initializes the RecyclerVew for the current job's bids
        recyclerView = (RecyclerView)findViewById(R.id.recyclerView);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        //Initializes the process dialog
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading Data");
        progressDialog.show();

        //Initialize the database reference
        bDatabaseReference = FirebaseDatabase.getInstance().getReference("bid");
        bDatabaseReference.addValueEventListener(new ValueEventListener()
        {
            @Override
            public void onDataChange(DataSnapshot snapshot)
            {
                //Grabs each element from the relevant database
                for (DataSnapshot dataSnapshot : snapshot.getChildren())
                {
                    //Grabs the current UserBid element
                    UserBid bid = dataSnapshot.getValue(UserBid.class);

                    //Checks that the bid jobref matches the current job
                    if(bid.getJobRef().equals(job.getDbRef()))
                    {
                        //Adds the current bid to the list of bids
                        bids.add(bid);
                    }
                }

                //Sets the adapter for reviews and passes the reviews to the adapter
                bAdapter = new BidsRecyclerViewAdapter(getApplicationContext(),bids);
                recyclerView.setAdapter(bAdapter);

                //Shows when loading data
                progressDialog.dismiss();
            }

            @Override
            public void onCancelled(DatabaseError databaseError)
            {

                //Cancels dispaly
                progressDialog.dismiss();

            }
        });
    }

    /**
     * Retrieves data sent with the intent in the extra field
     */
    private void getIncomingIntent()
    {
        Log.d(TAG, "getIncomingIntent: checking for incoming intent");

        //Checks if the intent has an extra with the reference job
        if(this.getIntent().hasExtra("job"))
        {
            Log.d(TAG, "getIncomingIntent: found intent extra");

            //Grabs the data in the extra
            job = (UserJob) this.getIntent().getSerializableExtra("job");
        }
    }



}