package uk.ac.tees.com2060.cupcake;



import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.ArrayList;
import java.util.List;

import static android.content.ContentValues.TAG;

/**
 * JobDetailsTab2Review Fragment
 */
public class JobDetailsTab2Review extends Fragment
{
    //Variables for layout access
    RecyclerView recyclerView;
    ReviewRecyclerViewAdapter rAdapter;

    //Variable for database reference
    DatabaseReference rDatabaseRef;

    //Variables for object storage
    UserJob job;
    List<Review> reviews = new ArrayList<>();

    /**
     * Initializes Fragment
     * @param savedInstanceState
     */
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        getIncomingIntent();
    }

    /**
     * Creates and returns the view hierarchy associated with the fragment
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return rootView
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View rootView = inflater.inflate(R.layout.fragment_tab2_reviews, container, false);

        //Initialize the RecyclerView for the user reviews
        recyclerView = (RecyclerView)rootView.findViewById(R.id.recyclerViewReviews);
        recyclerView.setHasFixedSize(false);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));

        //Initialize database reference
        rDatabaseRef = FirebaseDatabase.getInstance().getReference("review");
        rDatabaseRef.addValueEventListener(new ValueEventListener()
        {
            @Override
            public void onDataChange(DataSnapshot snapshot)
            {
                //Grabs each element from the relevant database
                for (DataSnapshot dataSnapshot : snapshot.getChildren())
                {
                    //Grabs the current review element
                    Review review = dataSnapshot.getValue(Review.class);

                    //Checks if the reviewee matches the job poster
                    if(review.getReviewee().equals(job.getPoster()))
                    {
                        //Adds the current review to the list of reviews
                        reviews.add(review);
                    }
                }

                //Sets the adapter for the current reviews and passes the reviews to the adapter
                rAdapter = new ReviewRecyclerViewAdapter(getContext(), reviews);
                recyclerView.setAdapter(rAdapter);
            }

            @Override
            public void onCancelled(DatabaseError databaseError)
            {

            }
        });
        return rootView;
    }

    /**
     * Retrieves data sent with the intent in the extra field
     */
    private void getIncomingIntent(){
        Log.d(TAG, "getIncomingIntent: checking for incoming intent");

        //Checks if the intent has an extra with the reference job
        if(getActivity().getIntent().hasExtra("job"))
        {
            Log.d(TAG, "getIncomingIntent: found intent extra");

            //Grabs the data in the extra
            job = (UserJob) getActivity().getIntent().getSerializableExtra("job");
        }
    }
}
