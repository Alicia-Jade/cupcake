package uk.ac.tees.com2060.cupcake;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;

import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import com.squareup.picasso.Picasso;

import de.hdodenhof.circleimageview.CircleImageView;

/**
 * Created by s6040531 on 19/03/18.
 */

/**
 * ExploreTab3Profile Fragment
 */
public class ExploreTab3Profile extends Fragment
{
    //Variable for database reference
    DatabaseReference databaseReference;

    //Variable for object storage
    User u;

    //Variable for Firebase access
    FirebaseAuth auth;

    //Variables for layout access
    TextView name;
    Button btnJobs, btnFeedback;
    Button btnEdit;
    CircleImageView image;

    /**
     * Initializes Fragment
     * @param savedInstanceState
     */
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
    }

    /**
     * Creates and returns the view hierarchy associated with the fragment
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return rootView
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View rootView = inflater.inflate(R.layout.fragment_tab3_profile, container, false);

        //Initialize widgets
        btnJobs = (Button)rootView.findViewById(R.id.btn_jobs);
        btnFeedback = (Button)rootView.findViewById(R.id.btn_feedback);
        btnEdit = (Button)rootView.findViewById(R.id.btn_edit_profile);
        image = (CircleImageView)rootView.findViewById(R.id.profile_image);
        name = (TextView) rootView.findViewById(R.id.tv_name);

        //Gets the current user
        auth = FirebaseAuth.getInstance();

        //Initialize database
        databaseReference = FirebaseDatabase.getInstance().getReference("user");

        //OnClickListener's for layout buttons
        btnJobs.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                //Loads the ViewMyJobsActivity
                startActivity(new Intent(getActivity(),ViewMyJobsActivity.class));
            }
        });

        btnEdit.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                //Loads the EditProfileActivity and passes it the user id
                Intent goToUpdate = new Intent(getActivity(), EditProfileActivity.class);
                goToUpdate.putExtra("USER_ID", u.getId());
                startActivity(goToUpdate);
            }
        });

        btnFeedback.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                //Loads the AllFeedbackActivity and passes the user
                Intent intent = new Intent(getActivity(),AllFeedbackActivity.class);
                intent.putExtra("user",u);
                startActivity(intent);
            }
        });

        databaseReference.addValueEventListener(new ValueEventListener()
        {
            @Override
            public void onDataChange(DataSnapshot snapshot)
            {
                //Grabs each element in the relevant database
                for (DataSnapshot dataSnapshot : snapshot.getChildren())
                {
                    //Checks that the current user element matches the current user
                    if(dataSnapshot.getKey().equals(auth.getCurrentUser().getUid()))
                    {
                        //Grabs the current user element
                        u = dataSnapshot.getValue(User.class);

                        //Sets the name textview and the profile image
                        name.setText(u.getName());
                        Picasso.with(getActivity()).load(u.getImage()).placeholder(R.mipmap.ic_launcher).into(image);
                    }
                }

            }

            @Override
            public void onCancelled(DatabaseError databaseError) {


            }
        });

        return rootView;

    }

    /**
     * Allows for subclass instantiation
     * @param view
     * @param savedInstanceState
     */
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);

    }
}
