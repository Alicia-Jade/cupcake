package uk.ac.tees.com2060.cupcake;

import android.os.Bundle;

/**
 * Privacy Settings Activity.
 */

public class PrivacyActivity extends AppCompatPreferenceActivity
{
    /**
     * Initializes activity.
     * @param savedInstanceState
     */
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        // Setting the view and links to activity_privacy.xml
        setContentView(R.layout.activity_privacy);
    }
}
