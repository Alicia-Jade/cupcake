package uk.ac.tees.com2060.cupcake;


import android.content.Intent;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import com.google.firebase.auth.FirebaseAuth;


/**
 * AddJobActivity Class
 */
public class AddJobActivity extends AppCompatActivity implements AddJobTab1Details.SendMessage, AddJobTab2Pickup.SendMessage
{
    //Variables used for layout access
    private ViewPagerAdapter viewPagerAdapter;
    private ViewPager viewPager;
    private TabLayout tabLayout;
    private Toolbar toolbar;
    private FirebaseAuth auth;

    /**
     * Initializes Activity
     * Retrieves any required layout widgets
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        //Sets up the required xml layout file
        setContentView(R.layout.activity_add_job);

        //Initializes the toolbar
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //Create the adapter that will return a fragment for each of the three
        //primary sections of the activity.
        viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());

        //Set up the ViewPager with the sections adapter and the TabLayout
        viewPager = (ViewPager) findViewById(R.id.viewPager);
        viewPager.setAdapter(viewPagerAdapter);
        tabLayout = (TabLayout) findViewById(R.id.tabs);

        //Sets action listeners to viewPager and tabLayout
        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(viewPager));
        tabLayout.clearOnTabSelectedListeners();
    }

    /**
     * Initializes the contents of the menu options provided
     * @param menu
     * @return true
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_add_job, menu);
        return true;
    }

    /**
     * Called when a menu option is selected and executes the relevant calls
     * @param item
     * @return menu item selected
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        //Gets the id of the selected option
        int id = item.getItemId();

        //Starts the activity which matches the id of the selected option
        if(id ==R.id.action_settings)
        {
            startActivity(new Intent(AddJobActivity.this,SettingsActivity.class));
            return true;
        }
        else if(id==R.id.action_logOut)
        {
            signOut();
            startActivity(new Intent(AddJobActivity.this, LoginActivity.class));
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Signs the current user out
     */
    public void signOut()
    {
        auth.signOut();
    }
    /**
     * Used to send data to tab2
     * @param userj
     */
    @Override
    public void sendData1(UserJob userj)
    {
        String tag = "android:switcher:" + R.id.viewPager + ":" + 1;
        AddJobTab2Pickup t2 = (AddJobTab2Pickup)getSupportFragmentManager().findFragmentByTag(tag);
        t2.dataReceived(userj);
    }

    /**
     * Used to send data to tab3
     * @param userj
     */
    @Override
    public void sendData2(UserJob userj)
    {
        String tag = "android:switcher:" + R.id.viewPager + ":" + 2;
        AddJobTab3Delivery t3 = (AddJobTab3Delivery) getSupportFragmentManager().findFragmentByTag(tag);
        t3.dataReceived(userj);
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class ViewPagerAdapter extends FragmentPagerAdapter
    {

        /**
         * Main constructor
         * @param fm
         */
        public ViewPagerAdapter(FragmentManager fm)
        {
            super(fm);
        }

        /**
         * Moves to selected tab
         * @param position
         * @return selected tab or null
         */
        @Override
        public Fragment getItem(int position)
        {
            //Finds the tab at the current position and returns a new instance of the selected tab
            switch (position){
                case 0:
                    AddJobTab1Details tab1 = new AddJobTab1Details();
                    return tab1;
                case 1:
                    AddJobTab2Pickup tab2 = new AddJobTab2Pickup();
                    return tab2;
                case 2:
                    AddJobTab3Delivery tab3 = new AddJobTab3Delivery();
                    return tab3;
                default:
                    return null;
            }
        }

        /**
         * Returns the number of tabs
         * @return 3
         */
        @Override
        public int getCount()
        {
            // Show 3 total pages.
            return 3;
        }

        /**
         * Grabs the title of the current tab
         * @param position
         * @return title
         */
        @Override
        public CharSequence getPageTitle(int position)
        {
            String title = null;

            //Uses the current position to find the tab title
            if (position == 0)
            {
                title = "Details";
            }
            else if (position == 1)
            {
                title = "Pickup";
            }
            else if(position == 2)
            {
                title = "Delivery";
            }
            return title;
        }
    }
}
