package uk.ac.tees.com2060.cupcake;

import android.app.ProgressDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;
import java.util.ArrayList;
import java.util.List;
import static android.content.ContentValues.TAG;

/**
 * AllFeedbackActivity Class
 */
public class AllFeedbackActivity extends AppCompatActivity
{
    //Variable for database reference
    DatabaseReference rDatabaseRef;

    //Variables for layout access
    ProgressDialog progressDialog;
    RecyclerView recyclerView;
    ReviewRecyclerViewAdapter adapter;

    //Variables for object storage
    List<Review> list = new ArrayList<>();
    User user;

    /**
     * Initializes Activity
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        //Sets up the required xml layout file
        setContentView(R.layout.activity_all_feedback);

        //Initializes the toolbar
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //Calls getIncomingIntent()
        getIncomingIntent();

        //Initializes the RecyclerView for the user's reviews
        recyclerView = (RecyclerView)findViewById(R.id.recyclerFeedback);
        recyclerView.setHasFixedSize(false);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        //Initializes the process dialog
        progressDialog = new ProgressDialog(this);
        progressDialog.setMessage("Loading Data...");
        progressDialog.show();

        //Initializes the database reference
        rDatabaseRef = FirebaseDatabase.getInstance().getReference("review");
        rDatabaseRef.addValueEventListener(new ValueEventListener()
        {
            @Override
            public void onDataChange(DataSnapshot snapshot)
            {
                //Grabs each element from the relevant database
                for (DataSnapshot dataSnapshot : snapshot.getChildren())
                {
                    //Grabs the current review element
                    Review review = dataSnapshot.getValue(Review.class);

                    //Checks that the current review element is for the user
                    if(review.getReviewee().equals(user.getId()))
                    {
                        //Adds the review element to the list of reviews
                        list.add(review);
                    }
                }

                //Sets the adapter for the reviews and passes the reviews to the adapter
                adapter = new ReviewRecyclerViewAdapter(getApplicationContext(), list);
                recyclerView.setAdapter(adapter);

                //Closes the dialog
                progressDialog.dismiss();
            }

            @Override
            public void onCancelled(DatabaseError databaseError)
            {
                //Closes the dialog
                progressDialog.dismiss();

            }
        });
    }

    /**
     * Retrieves data sent with the intent in the extra field
     */
    private void getIncomingIntent()
    {
        Log.d(TAG, "getIncomingIntent: checking for incoming intent");

        //Checks if the intent has an extra with the reference user
        if(this.getIntent().hasExtra("user"))
        {
            Log.d(TAG, "getIncomingIntent: found intent extra");

            //Grabs the data in the extra
            user = (User) this.getIntent().getSerializableExtra("user");
        }
    }
}
