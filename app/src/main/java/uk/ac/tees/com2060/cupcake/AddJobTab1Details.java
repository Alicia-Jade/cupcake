package uk.ac.tees.com2060.cupcake;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.os.Bundle;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.*;

import com.google.firebase.auth.FirebaseAuth;

/**
 * AddJobTab1Details Fragment
 */
public class AddJobTab1Details extends Fragment
{
    //Variables for object storage
    public SendMessage SM;
    public FirebaseAuth auth;
    public String id;
    private UserJob uj;

    //Variables for layout access
    private ViewPager viewPager;
    private Button btnNext, btnCancel;
    private EditText etTitle;
    private EditText etDesc;
    private EditText etWeight;

    /**
     * Initializes Fragment
     * @param savedInstanceState
     */
    @Override
    public void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
    }

    /**
     * Creates and returns the view hierarchy associated with the fragment
     * @param inflater
     * @param container
     * @param savedInstanceState
     * @return rootView
     */
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View rootView = inflater.inflate(R.layout.fragment_tab1_details, container, false);
        return rootView;
    }

    /**
     * Allows for subclass instantiation
     * Retrieves any required layout widgets
     * @param view
     * @param savedInstanceState
     */
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState)
    {
        super.onViewCreated(view, savedInstanceState);

        //Initialize widgets
        btnNext = (Button)view.findViewById(R.id.btn_next1);
        btnCancel = (Button)view.findViewById(R.id.btn_cancel1);
        viewPager = (ViewPager)getActivity().findViewById(R.id.viewPager);
        etTitle = (EditText)view.findViewById(R.id.et_jobTitle);
        etDesc = (EditText)view.findViewById(R.id.et_description);
        etWeight = (EditText)view.findViewById(R.id.et_weight);

        //OnClickListener's for layout buttons
        btnNext.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                //Grabs the id of the current user and the filled out details for the job details
                //section and saves them as a UserJob object
                //Passes the UserJob object to the next tab and displays the next tab
                auth = FirebaseAuth.getInstance();
                id = auth.getCurrentUser().getUid();
                uj = new UserJob(id, etTitle.getText().toString(), etDesc.getText().toString(),Double.parseDouble(etWeight.getText().toString()), "live", null);
                SM.sendData1(uj);

                viewPager.setCurrentItem(1);

            }
        });

        btnCancel.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View v)
            {
                //Goes back to the Explore Activity
                startActivity(new Intent(getActivity(), ExploreActivity.class));
            }
        });

    }

    /**
     * Interface for SendMessage
     */
    interface SendMessage
    {
        void sendData1(UserJob userj);
    }

    /**
     * Called when fragment is first attached to its context
     * @param context
     */
    @Override
    public void onAttach(Context context)
    {
        super.onAttach(context);

        try
        {
            SM = (SendMessage)getActivity();
        }
        catch(ClassCastException e)
        {
            throw new ClassCastException("Error in retrieving data. Please try again.");
        }
    }
}
