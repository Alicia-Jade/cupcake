package uk.ac.tees.com2060.cupcake;

import java.io.Serializable;

/**
 * Model class used to represent a single address
 */
public class JAddress implements Serializable
{
    /**
     * Attributes of JAddress
     */
    public int hNum;
    public String street;
    public String add;
    public String city;
    public String county;
    public String postcode;
    public LatLng loc;

    /**
     * Empty constructor
     */
    public JAddress(){}

    /**
     * Main constructor
     * @param n
     * @param s
     * @param a
     * @param ci
     * @param co
     * @param p
     * @param l
     */
    public JAddress(int n, String s, String a, String ci, String co, String p, LatLng l)
    {
        hNum = n;
        street = s;
        add = a;
        city = ci;
        county = co;
        postcode = p;
        loc = l;
    }

    /**
     * Retrieves the house number
     * @return hNum
     */
    public int getNum()
    {
        return hNum;
    }

    /**
     * Retrieves the street name
     * @return street
     */
    public String getStreet()
    {
        return street;
    }

    /**
     * Retrieves the additional address detail
     * @return add
     */
    public String getAdditional()
    {
        return add;
    }

    /**
     * Retrieves the city
     * @return city
     */
    public String getCity()
    {
        return city;
    }

    /**
     * Retrieves the county
     * @return county
     */
    public String getCounty()
    {
    return county;
    }

    /**
     * Retrieves the postcode
     * @return postcode
     */
    public String getPostcode()
    {
        return postcode;
    }

    /**
     * Retrieves the location object, consisting of latitude and longitude
     * @return loc
     */
    public LatLng getLoc() {
        return loc;
    }
}
