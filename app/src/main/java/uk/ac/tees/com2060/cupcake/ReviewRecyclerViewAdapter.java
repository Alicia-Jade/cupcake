package uk.ac.tees.com2060.cupcake;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.firebase.database.DataSnapshot;
import com.google.firebase.database.DatabaseError;
import com.google.firebase.database.DatabaseReference;
import com.google.firebase.database.FirebaseDatabase;
import com.google.firebase.database.ValueEventListener;

import java.util.List;

import static android.content.ContentValues.TAG;

/**
 * ReviewRecyclerAdapter Class
 */
public class ReviewRecyclerViewAdapter extends RecyclerView.Adapter<ReviewRecyclerViewAdapter.ViewHolder>
{
    //Variable to store context class is called from
    Context context;

    //Variable for database reference
    DatabaseReference uDatabaseReference;

    //Variable to store list of Reviews
    List<Review> reviewList;

    /**
     * Main constructor
     * @param context
     * @param tempList
     */
    public ReviewRecyclerViewAdapter(Context context, List<Review> tempList)
    {
        this.reviewList= tempList;
        this.context = context;
    }

    /**
     * ViewHolder Class
     */
    class ViewHolder extends RecyclerView.ViewHolder
    {

        //Variables used for layout access
        public TextView rReviewBy;
        public TextView rRating;
        public TextView rReview;
        RelativeLayout parentLayout;

        /**
         * Main constructor
         * @param itemView
         */
        public ViewHolder(View itemView)
        {
            super(itemView);

            //Initialize widgets
            rReviewBy = (TextView) itemView.findViewById(R.id.txt_reviewby);
            rRating = (TextView) itemView.findViewById(R.id.txt_rating);
            rReview = (TextView) itemView.findViewById(R.id.txt_review);
            parentLayout = itemView.findViewById(R.id.parent_layout);
        }
    }

    /**
     * Creates a new RecyclerView.ViewHolder
     * @param parent
     * @param viewType
     * @return viewHolder
     */
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.recyclerview_feedback, parent, false);
        ViewHolder viewHolder = new ViewHolder(view);
        return viewHolder;
    }

    /**
     * Updates the contents of the RecyclerView.ViewHolder
     * @param holder
     * @param position
     */
    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position)
    {
        Log.d(TAG, "onBindViewHolder: called.");

        //Grabs the review at the current position
        final Review review = reviewList.get(position);

        //Initializes the database reference
        uDatabaseReference = FirebaseDatabase.getInstance().getReference("user");
        uDatabaseReference.addValueEventListener(new ValueEventListener()
        {
            @Override
            public void onDataChange(DataSnapshot snapshot)
            {
                //Grabs each element from the relevant database
                for (DataSnapshot dataSnapshot : snapshot.getChildren())
                {
                    //Checks that the current user element matches the reviewee
                    if(dataSnapshot.getKey().equals(review.getReviewer()))
                    {
                        //Grabs the current user element
                        //Sets the name textView
                        User u = dataSnapshot.getValue(User.class);
                        holder.rReviewBy.setText(u.getName());
                    }
                }
            }

            @Override
            public void onCancelled(DatabaseError databaseError)
            {

            }
        });

        //Sets the textviews
        holder.rRating.setText(review.getRating() + " / 10");
        holder.rReview.setText(review.getReview());
    }

    /**
     * Returns the amount of jobs
     * @return
     */
    @Override
    public int getItemCount()
    {
        return reviewList.size();
    }

}
