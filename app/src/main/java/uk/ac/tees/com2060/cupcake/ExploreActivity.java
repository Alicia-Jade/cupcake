package uk.ac.tees.com2060.cupcake;

/**
 * The Haul of Kings application allows user to post delivery jobs, which other users can then bid on
 *
 * @authors Alicia-Jade Fisher, Heidi Portwine, Nathan Hanlon, Connor Bassett
 * @version 8.0.0
 * @since   09-02-2018
 */

import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

/**
 * ExploreActivity Class
 */

public class ExploreActivity extends AppCompatActivity
{
    // Variables used for Firebase access
    private FirebaseAuth.AuthStateListener authListener;
    private FirebaseAuth auth;

    // Variable used for layout access
    private ViewPagerAdapter viewPagerAdapter;
    private ViewPager viewPager;
    private TabLayout tabLayout;
    private Toolbar toolbar;

    /**
     * Initializes Activity
     * Retrieves any required layout widgets
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        //Sets up the required xml layout file
        setContentView(R.layout.activity_explore);

        //Initializes the toolbar
        toolbar =(Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //Gets a reference to the currently logged in user
        auth = FirebaseAuth.getInstance();
        final FirebaseUser user = FirebaseAuth.getInstance().getCurrentUser();

        authListener = new FirebaseAuth.AuthStateListener()
        {
            @Override
            public void onAuthStateChanged(@NonNull FirebaseAuth firebaseAuth)
            {
                FirebaseUser user = firebaseAuth.getCurrentUser();
                if (user == null)
                {
                    //User auth state is changed - user is null
                    //launch login activity
                    startActivity(new Intent(ExploreActivity.this, LoginActivity.class));
                    finish();
                }
            }
        };

        //Create the adapter that will return a fragment for each of the three
        //primary sections of the activity.
        viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());

        //Set up the ViewPager with the sections adapter.
        viewPager = (ViewPager) findViewById(R.id.viewPager);
        viewPager.setAdapter(viewPagerAdapter);

        tabLayout = (TabLayout) findViewById(R.id.eTabs);

        viewPager.addOnPageChangeListener(new TabLayout.TabLayoutOnPageChangeListener(tabLayout));
        tabLayout.addOnTabSelectedListener(new TabLayout.ViewPagerOnTabSelectedListener(viewPager));
        viewPager.setCurrentItem(1, false);

    }

    /**
     * Initializes the contents of the menu options provided
     * @param menu
     * @return true
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu)
    {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }


    /**
     * Called when a menu option is selected and executes the relevant calls
     * @param item
     * @return menu item selected
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item)
    {
        //Gets the id of the selected option
        int id = item.getItemId();

        //Starts the activity which matches the id of the selected option
        if(id == R.id.action_addJob)
        {
            startActivity(new Intent(ExploreActivity.this, AddJobActivity.class));
            return true;
        }
        else if(id ==R.id.action_settings)
        {
            startActivity(new Intent(ExploreActivity.this,SettingsActivity.class));
            return true;
        }
        else if(id==R.id.action_logOut)
        {
            signOut();
            startActivity(new Intent(ExploreActivity.this, LoginActivity.class));
            finish();
        }
        return super.onOptionsItemSelected(item);
    }

    /**
     * Signs the current user out
     */
    public void signOut()
    {
        auth.signOut();
    }

    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class ViewPagerAdapter extends FragmentPagerAdapter
    {

        /**
         * Main constructor
         * @param fm
         */
        public ViewPagerAdapter(FragmentManager fm)
        {
            super(fm);
        }

        /**
         * Moves to selected tab
         * @param position
         * @return selected tab or null
         */
        @Override
        public Fragment getItem(int position)
        {

            //Finds the tab at the current position and returns a new instance of the selected tab
            switch (position){
                case 0:
                    ExploreTab1Inbox tab1 = new ExploreTab1Inbox();
                    return tab1;
                case 1:
                    ExploreTab2Explore tab2 = new ExploreTab2Explore();
                    return tab2;
                case 2:
                    ExploreTab3Profile tab3 = new ExploreTab3Profile();
                    return tab3;
                default:
                    return null;
            }
        }

        /**
         * Returns the number of tabs
         * @return 3
         */
        @Override
        public int getCount() {
            // Show 3 total pages.
            return 3;
        }

        /**
         * Grabs the title of the current tab
         * @param position
         * @return title
         */
        @Override
        public CharSequence getPageTitle(int position)
        {
            String title = null;

            //Uses the current position to find the tab title
            if (position == 0)
            {
                title = "Inbox";
            }
            else if (position == 1)
            {
                title = "Explore";
            }
            else if(position == 2)
            {
                title = "Profile";
            }
            return title;
        }
    }

}
