package uk.ac.tees.com2060.cupcake;

import android.os.Bundle;

/**
 * Anything Else Settings Activity.
 */
public class AnythingElseActivity extends AppCompatPreferenceActivity
{
    /**
     * Initializes activity.
     * @param savedInstanceState
     */
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        // Setting the view and links to activity_anything.xml
        setContentView(R.layout.activity_anything);
    }
}
